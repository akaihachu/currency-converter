package com.testtestonly.ViddseeApp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.TypedArray
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.testtestonly.ViddseeApp.Models.FullCurrencyModel
import com.testtestonly.ViddseeApp.Presenter.MainPresenter
import com.testtestonly.ViddseeApp.Presenter.MainPresenterImp
import com.testtestonly.ViddseeApp.Utils.*
import com.testtestonly.ViddseeApp.Views.ListActivity
import com.testtestonly.ViddseeApp.Views.MainView
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DecimalFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.*

class MainActivity : MainView, AppCompatActivity() {
    lateinit var presenter: MainPresenter<MainView>
    var keyboard = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenterImp()
        presenter.onAttach(this)
        initUIs()
    }

    fun initUIs() {
        start_amount_edt.requestFocus()
        start_amount_edt.let {
            it.addTextChangedListener(CommonUtils.DecimalEditextFormat(it, { it ->
                presenter.onTextChange(it)
            }))
        }

        start_btn.setText(
            PreferenceUtils.startConversion
        )
        local_btn.setText(
            PreferenceUtils.localConversion
        )

        start_btn.setOnClickListener {
            startListActivity(PreferenceUtils.startConversion, true)
        }
        local_btn.setOnClickListener {
            startListActivity(PreferenceUtils.localConversion, false)
        }
        keyboard_btn.setOnClickListener({
            if (keyboard) {
                hideKeyboard()
                keyboard = false
            } else {
                keyboard = true
                showKeyboard()
            }
        })
        clear_btn.setOnClickListener({
            clearCurrency(it)
        })
    }

    fun startListActivity(focus: String?, startCurrency: Boolean) {
        val intent = Intent(this@MainActivity, ListActivity::class.java)
        intent.putExtra(Constants.INTENT_FOCUS_CURRENCY, focus)
        if (startCurrency) {
            startLauncher.launch(intent)
        } else {
            localLauncher.launch(intent)
        }
    }
    var startLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                data?.let {
                    doStartButtonSelection(it.getStringExtra(Constants.INTENT_EXTRA))
                }
            }
        }
    var localLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                data?.let {
                    doLocalButtonSelection(it.getStringExtra(Constants.INTENT_EXTRA))
                }
            }
        }

    fun doStartButtonSelection(currency: String?) {
        PreferenceUtils.startConversion = currency
        start_btn.setText(currency)
        presenter.onStartButtonSelect(
            currency!!,
            CommonUtils.getDoubleStartString(start_amount_edt.text.toString())
        )
    }

    fun doLocalButtonSelection(pos: String?) {
        PreferenceUtils.localConversion = pos
        local_btn.setText(pos)
        presenter.onLocalButtonSelect(
            pos!!,
            CommonUtils.getDoubleStartString(start_amount_edt.text.toString())
        )
    }

    fun convertCurrency(view: View) {
        val startDollars: Double
        val localAmount: Double
        val validDouble = "^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$"
        val currencyFormat = DecimalFormat.getInstance(Locale.US)
        (currencyFormat as DecimalFormat).applyPattern(Constants.LOCAL_PARTTEN_DECIMAL)

        val cdnTextValue = start_amount_edt.text.toString().replace(",", "")

        if (!TextUtils.isEmpty(cdnTextValue) && cdnTextValue.matches(validDouble.toRegex())) {
            startDollars = java.lang.Double.parseDouble(cdnTextValue)
            localAmount = presenter.convertClick(startDollars)
            local_amount_txt.text = currencyFormat.format(localAmount)
        }
    }

    fun clearCurrency(view: View) {
        start_amount_edt.text?.clear()
        local_amount_txt.text = ""
    }

    override fun onAPISuccess(model: FullCurrencyModel) {
        model.rates?.let {
            PreferenceUtils.currencyModel = it
        }
    }

    override fun onResume() {
        if (CommonUtils.checkIfTimeToDownload()) {
            presenter.getAPIRates()
        }
        super.onResume()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }


    override fun onAPIFailure() {

    }

    override fun upDateLocalAmount(amout: Double) {
        val currencyFormat = DecimalFormat.getInstance(Locale.US)
        (currencyFormat as DecimalFormat).applyPattern(Constants.LOCAL_PARTTEN_DECIMAL)
        local_amount_txt.text = currencyFormat.format(amout)
    }

}
