package com.testtestonly.ViddseeApp.APIs


import com.testtestonly.ViddseeApp.Models.FullCurrencyModel
import com.testtestonly.ViddseeApp.Models.FullSymbolModel
import com.testtestonly.ViddseeApp.Utils.Constants
import retrofit2.http.*
import retrofit2.http.GET
import retrofit2.Call


interface ApiInterface {
    @POST("latest")
    fun  getLatestRates(@Query(Constants.ACCESS_KEY) access_key:String?=Constants.REGISTER_KEY):Call<FullCurrencyModel>
    @POST("symbols")
    fun  getSymbols(@Query(Constants.ACCESS_KEY) access_key:String?=Constants.REGISTER_KEY):Call<FullSymbolModel>
}