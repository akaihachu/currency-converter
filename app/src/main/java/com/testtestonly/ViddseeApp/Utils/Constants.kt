package com.testtestonly.ViddseeApp.Utils

class Constants {
    companion object {
        val SHARED_CURRENCY_MODEL="sharedModel_NEW"
        val SHARED_SYMBOL_MODEL="sharedSymbolModel_NEW"
        val SHARED_START_CURRENCY="SHARED_START_CURRENCY_NEW"
        val SHARED_LOCAL_CURRENCY="SHARED_LOCAL_CURRENCY_NEW"
        val INTENT_FOCUS_CURRENCY="INTENT_FOCUS_CURRENCY_NEW"
        val START_PARTTEN_DECIMAL="#,###"
        val LOCAL_PARTTEN_DECIMAL="#,###.00"
        val API_URL_NEW = "http://api.exchangeratesapi.io/v1/"
        const val ACCESS_KEY = "access_key"
        val REGISTER_KEY = "59ee8f035ba56129fb06bb2d9606e273"
        val INTENT_EXTRA="INTENT_EXTRA_NEW"
    }
}