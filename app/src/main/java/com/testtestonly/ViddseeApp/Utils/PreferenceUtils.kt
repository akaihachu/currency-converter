package com.testtestonly.ViddseeApp.Utils

import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.testtestonly.ViddseeApp.Base.TinyDB.Companion.tinyDb
import com.testtestonly.ViddseeApp.Models.CurrencyModel
import com.testtestonly.ViddseeApp.Models.SymbolModel

object PreferenceUtils {

    var startConversion: String?
        get() = tinyDb!!.getString(Constants.SHARED_START_CURRENCY, "USD")
        set(value) {
            tinyDb!!.putString(Constants.SHARED_START_CURRENCY, value)
        }
    var localConversion: String?
        get() = tinyDb!!.getString(Constants.SHARED_LOCAL_CURRENCY, "SGD")
        set(value) {
            tinyDb!!.putString(Constants.SHARED_LOCAL_CURRENCY, value)
        }
    var symbolModel: SymbolModel?
        get() {
            val jsonSymbolModel = tinyDb!!.getString(Constants.SHARED_SYMBOL_MODEL)
            if (!TextUtils.isEmpty(jsonSymbolModel)) {
                var gson = Gson()
                var mModel = gson.fromJson(
                    jsonSymbolModel,
                    object : TypeToken<SymbolModel>() {}.type
                ) as SymbolModel
                return mModel
            } else {
                return SymbolModel()
            }
        }
        set(data) {
            val json = Gson().toJson(data)
            data?.let {
                tinyDb!!.putString(Constants.SHARED_SYMBOL_MODEL, json)
            }
        }

    var currencyModel: CurrencyModel
        get() {
            val jsonModel = tinyDb!!.getString(Constants.SHARED_CURRENCY_MODEL)
            if (!TextUtils.isEmpty(jsonModel)) {
                var gson = Gson()
                var mModel = gson.fromJson(
                    jsonModel,
                    object : TypeToken<CurrencyModel>() {}.type
                ) as CurrencyModel
                return mModel
            } else {
                return CurrencyModel()
            }
        }
        set(data) {
            val json = Gson().toJson(data)
            data?.let {
                tinyDb!!.putString(Constants.SHARED_CURRENCY_MODEL, json)
            }
        }
}