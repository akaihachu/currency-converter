package com.testtestonly.ViddseeApp.Utils

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.EditText
import android.widget.Toast
import com.testtestonly.ViddseeApp.R
import com.testtestonly.ViddseeApp.Utils.Constants.Companion.START_PARTTEN_DECIMAL
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class CommonUtils {

    class DecimalEditextFormat(private val editText: EditText, val doNext: (Double) -> Unit) :
        TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
        override fun afterTextChanged(editable: Editable) {
            editText.removeTextChangedListener(this)
            val separator = "."
            val thousandSeperator = ","
            var s = editText.text.toString()
            s = s.replace(thousandSeperator.toString(), "")
            var intPart = ""
            var decimalPart = ""

            if (s.length > 0) {
                if (s.indexOf(separator) == 0) {
                    intPart = "0"
                    decimalPart = s
                } else if (s.indexOf(separator) == -1) {
                    intPart = s
                } else {
                    intPart = s.substring(0, s.indexOf(separator))
                    decimalPart = s.substring(s.indexOf(separator), s.length)
                }
                val sdd = DecimalFormat.getInstance(Locale.US)
                (sdd as DecimalFormat).applyPattern(START_PARTTEN_DECIMAL)
//                val sdd = DecimalFormat(START_PARTTEN_DECIMAL)
                val longNumber = intPart.toLong()
                val format = sdd.format(longNumber)
                editText.setText(format + decimalPart)
                editText.setSelection((format + decimalPart).length)
                doNext(getDoubleStartString(editText.text.toString()))
            } else {
                doNext(getDoubleStartString("0"))
            }
            editText.addTextChangedListener(this)
        }
    }

    companion object {
        fun getDoubleStartString(str: String): Double {
            var value = 0.0
            val separator = "."
            val thousandSeperator = ","
            var temp = str.replace(thousandSeperator.toString(), "")
            if (!TextUtils.isEmpty(temp)) {
                if (temp.indexOf(separator) == 0) {
                    temp = "0" + temp
                }

                try {
                    value = temp.toDouble()
                } catch (e: NumberFormatException) {
                    value = 0.0
                }
            }
            return value
        }

        fun test(context: Context) {
            context.getString(R.string.warning_offline)
        }

        protected const val DOWNLOAD_TIME_INTERVAL: Long = 3600000
        protected const val DOWNLOAD_TIME_INTERVAL_SYMBOL: Long = 36000000
        protected var mLastDownloadTime: Long = 0

        @JvmStatic
        fun checkIfTimeToDownloadSymbol(): Boolean {
            val now = System.currentTimeMillis()
            if (now - mLastClickTime < DOWNLOAD_TIME_INTERVAL_SYMBOL) {
                return false
            }
            mLastClickTime = now
            return true
        }
        @JvmStatic
        fun checkIfTimeToDownload(): Boolean {
            val now = System.currentTimeMillis()
            if (now - mLastClickTime < DOWNLOAD_TIME_INTERVAL) {
                return false
            }
            mLastClickTime = now
            return true
        }

        protected const val CLICK_TIME_INTERVAL: Long = 800
        protected var mLastClickTime: Long = 0

        @JvmStatic
        fun checkClickTimeValidation(): Boolean {
            val now = System.currentTimeMillis()
            if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                return false
            }
            mLastClickTime = now
            return true
        }
    }
}