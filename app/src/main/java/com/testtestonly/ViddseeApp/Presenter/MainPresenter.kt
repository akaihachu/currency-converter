package com.testtestonly.ViddseeApp.Presenter

import android.content.Context
import android.content.SharedPreferences
import com.testtestonly.ViddseeApp.Views.MainView

interface MainPresenter<V: MainView> {
    var mContext: Context
    var mView: V?
    fun onAttach(view: V?)
    fun onDestroy()
    fun getAPIRates()
    fun getAPISymbols()
    fun onTextChange(value:Double)
    fun convertClick(value:Double):Double
    fun clearClick()
    fun onStartButtonSelect(pos:String, startValue:Double)
    fun onLocalButtonSelect(pos:String, startValue: Double)
}