package com.testtestonly.ViddseeApp.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.content.res.TypedArray
import android.os.Build
import androidx.annotation.RequiresApi
import com.testtestonly.ViddseeApp.APIs.ApiClient
import com.testtestonly.ViddseeApp.Models.CurrencyModel
import com.testtestonly.ViddseeApp.Models.FullCurrencyModel
import com.testtestonly.ViddseeApp.Models.FullSymbolModel
import com.testtestonly.ViddseeApp.R
import com.testtestonly.ViddseeApp.Utils.CommonUtils
import com.testtestonly.ViddseeApp.Utils.PreferenceUtils
import com.testtestonly.ViddseeApp.Utils.getDouble
import com.testtestonly.ViddseeApp.Views.MainView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit

class MainPresenterImp<V : MainView> : MainPresenter<V> {
    private var view: V? = null
    var startConversion: Float = 1.0F
    var localConversion: Float = 1.0F
    lateinit private var context: Context

    lateinit var model:CurrencyModel

    override fun onAttach(view: V?) {
        this.view = view
        context = view as Context
        updateValues(PreferenceUtils.currencyModel)

        if (CommonUtils.checkIfTimeToDownload()) {
            getAPIRates()
        }

        if (CommonUtils.checkIfTimeToDownloadSymbol()){
            getAPISymbols()
        }
    }


    fun updateValues(mdl: CurrencyModel) {
        model=mdl
        PreferenceUtils.startConversion?.let { startConversion=model.getRateByCurrency(it) }
        PreferenceUtils.localConversion?.let { localConversion=model.getRateByCurrency(it) }

    }

    override fun onDestroy() {
        mView = null
    }

    override var mView: V?
        get() = view
        set(value) {}
    override var mContext: Context
        get() = context
        set(value) {}

    override fun getAPIRates() {
        var temp = ApiClient.`interface`
        var apiService: Call<FullCurrencyModel> = temp.getLatestRates()
        apiService.enqueue(object : Callback<FullCurrencyModel> {
            override fun onResponse(
                call: Call<FullCurrencyModel>?,
                response: retrofit2.Response<FullCurrencyModel>?
            ) {
                response?.let {
                    if (it.isSuccessful) {
                        it.body().rates?.let { it1 -> updateValues(it1) }
                        mView?.onAPISuccess(response.body())
                    }
                }
            }

            override fun onFailure(call: Call<FullCurrencyModel>?, t: Throwable?) {
                mView?.onAPIFailure()

            }
        })
    }
    override fun getAPISymbols() {
        var temp = ApiClient.`interface`
        var apiService: Call<FullSymbolModel> = temp.getSymbols()
        apiService.enqueue(object :Callback<FullSymbolModel>{
            override fun onResponse(
                call: Call<FullSymbolModel>?,
                response: Response<FullSymbolModel>?
            ) {
                response?.let{
                    if(it.isSuccessful){
                        PreferenceUtils.symbolModel=it.body().symbols
                    }
                }
            }

            override fun onFailure(call: Call<FullSymbolModel>?, t: Throwable?) {
                mView?.onAPIFailure()
            }
        })
   }

    override fun convertClick(value: Double): Double {
        return value * (localConversion.div(startConversion))
    }

    override fun clearClick() {
    }


    override fun onStartButtonSelect(pos: String, startValue: Double) {
        startConversion = model.getRateByCurrency(pos)
        var localAmount: Double = startValue * localConversion / startConversion
        mView?.upDateLocalAmount(localAmount)
    }

    override fun onLocalButtonSelect(pos: String, startValue: Double) {
        localConversion = model.getRateByCurrency(pos)
        var localAmount: Double = startValue * localConversion / startConversion
        mView?.upDateLocalAmount(localAmount)
    }


    override fun onTextChange(value: Double) {
        var localAmount: Double = value * localConversion / startConversion
        mView?.upDateLocalAmount(localAmount)
    }
}