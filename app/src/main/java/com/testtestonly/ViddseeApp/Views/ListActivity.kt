package com.testtestonly.ViddseeApp.Views

import android.app.Activity
import android.content.Intent
import android.content.res.TypedArray
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.testtestonly.ViddseeApp.R
import com.testtestonly.ViddseeApp.Utils.Constants
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {
    lateinit var keys: TypedArray
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        initViews()
    }
    fun initViews(){
        keys = resources.obtainTypedArray(R.array.key_array1)
        recycler.layoutManager=LinearLayoutManager(this)
        recycler.adapter=CurrencyAdapter(keys,intent.getStringExtra(Constants.INTENT_FOCUS_CURRENCY),{
            var finishIntent= Intent()
            finishIntent.putExtra(Constants.INTENT_EXTRA,it)
            setResult(Activity.RESULT_OK,finishIntent)
            onBackPressed()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_done, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId.equals(R.id.menu_done)){
            onBackPressed()
            return true
        } else{
            return super.onOptionsItemSelected(item)
        }
    }
}