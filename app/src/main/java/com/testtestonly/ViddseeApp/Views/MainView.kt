package com.testtestonly.ViddseeApp.Views

import android.content.SharedPreferences
import android.content.res.TypedArray
import com.testtestonly.ViddseeApp.Models.FullCurrencyModel

interface MainView {
    fun onAPISuccess(model:FullCurrencyModel)
    fun onAPIFailure()
    fun upDateLocalAmount(amout:Double)
}