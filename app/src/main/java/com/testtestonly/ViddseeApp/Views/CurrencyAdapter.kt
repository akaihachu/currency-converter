package com.testtestonly.ViddseeApp.Views

import android.content.res.TypedArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testtestonly.ViddseeApp.R
import com.testtestonly.ViddseeApp.Utils.PreferenceUtils
import kotlinx.android.synthetic.main.item_currency.view.*

class CurrencyAdapter(val keys:TypedArray,val focus:String?,val listener:(String?)->Unit):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Holder).onBind(position)
    }

    override fun getItemCount(): Int {
        return keys.length()
    }
    inner class Holder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun onBind(pos:Int){
            var currencyFullName=""
            PreferenceUtils.symbolModel?.let {
                currencyFullName=it.getNameBySymbol(keys.getString(pos))
            }
            itemView.long_name_txt.text=currencyFullName
            itemView.short_name_txt.text=keys.getString(pos)
            if (focus.equals(keys.getString(pos))){
                itemView.check_img.visibility=View.VISIBLE
            } else {
                itemView.check_img.visibility=View.INVISIBLE
            }
            itemView.root.setOnClickListener {
                listener(keys.getString(pos))
            }
        }
    }
}