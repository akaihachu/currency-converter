package com.testtestonly.ViddseeApp.Base

import android.app.Application.ActivityLifecycleCallbacks
import com.testtestonly.ViddseeApp.Base.App
import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.util.Log

class App : Application(), ActivityLifecycleCallbacks {
    override fun onCreate() {
        super.onCreate()
    }

    override fun onActivityCreated(activity: Activity, bundle: Bundle?) {
        Companion.activity = activity
    }

    override fun onActivityStarted(activity: Activity) {
        Companion.activity = activity
    }

    override fun onActivityResumed(activity: Activity) {}
    override fun onActivityPaused(activity: Activity) {}
    override fun onActivityStopped(activity: Activity) {}
    override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {}
    override fun onActivityDestroyed(activity: Activity) {}

    companion object {
        var instance: App? = null
            private set
        var activity: Activity? = null
            private set
        val appContext: Context
            get() = instance as App
    }

    init {
        instance = this
    }
}