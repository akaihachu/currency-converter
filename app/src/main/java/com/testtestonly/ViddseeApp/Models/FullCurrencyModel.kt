package com.testtestonly.ViddseeApp.Models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class FullCurrencyModel():Parcelable {
    @SerializedName("success")
    var success:Boolean = false

    @SerializedName("timestamp")
    var timestamp:Long = 0

    @SerializedName("base")
    var base:String = "EUR"

    @SerializedName("date")
    var date:String = "2021-11-02"

    @SerializedName("rates")
    var rates:CurrencyModel? = CurrencyModel()

    constructor(parcel: Parcel) : this() {
        success = parcel.readByte() != 0.toByte()
        timestamp = parcel.readLong()
        base = parcel.readString()!!
        date = parcel.readString()!!
        rates = parcel.readParcelable(CurrencyModel::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (success) 1 else 0)
        parcel.writeLong(timestamp)
        parcel.writeString(base)
        parcel.writeString(date)
        parcel.writeParcelable(rates, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FullCurrencyModel> {
        override fun createFromParcel(parcel: Parcel): FullCurrencyModel {
            return FullCurrencyModel(parcel)
        }

        override fun newArray(size: Int): Array<FullCurrencyModel?> {
            return arrayOfNulls(size)
        }
    }

}