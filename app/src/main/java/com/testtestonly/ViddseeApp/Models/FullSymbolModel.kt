package com.testtestonly.ViddseeApp.Models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class FullSymbolModel():Parcelable {
    @SerializedName("success")
    var success:Boolean = false
    @SerializedName("symbols")
    var symbols=SymbolModel()

    constructor(parcel: Parcel) : this() {
        success = parcel.readByte() != 0.toByte()
        symbols = parcel.readParcelable(SymbolModel::class.java.classLoader)!!
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel, p1: Int) {
        p0.writeByte(if (success) 1 else 0)
        p0.writeParcelable(symbols, p1)
    }

    companion object CREATOR : Parcelable.Creator<FullSymbolModel> {
        override fun createFromParcel(parcel: Parcel): FullSymbolModel {
            return FullSymbolModel(parcel)
        }

        override fun newArray(size: Int): Array<FullSymbolModel?> {
            return arrayOfNulls(size)
        }
    }
}