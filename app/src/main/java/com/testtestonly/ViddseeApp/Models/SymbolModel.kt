package com.testtestonly.ViddseeApp.Models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class SymbolModel() : Parcelable {


    @SerializedName("AED")
    var AED = "United Arab Emirates Dirham"

    @SerializedName("AFN")
    var AFN = "Afghan Afghani"

    @SerializedName("ALL")
    var ALL = "Albanian Lek"

    @SerializedName("AMD")
    var AMD = "Armenian Dram"

    @SerializedName("ANG")
    var ANG = "Netherlands Antillean Guilder"

    @SerializedName("AOA")
    var AOA = "Angolan Kwanza"

    @SerializedName("ARS")
    var ARS = "Argentine Peso"


    @SerializedName("AUD")
    var AUD = "Australian Dollar"

    @SerializedName("AWG")
    var AWG = "Aruban Florin"

    @SerializedName("AZN")
    var AZN = "Azerbaijani Manat"

    @SerializedName("BAM")
    var BAM = "Bosnia-Herzegovina Convertible Mark"

    @SerializedName("BBD")
    var BBD = "Barbadian Dollar"

    @SerializedName("BDT")
    var BDT = "Bangladeshi Taka"


    @SerializedName("BGN")
    var BGN = "Bulgarian Lev"

    @SerializedName("BHD")
    var BHD = "Bahraini Dinar"

    @SerializedName("BIF")
    var BIF = "Burundian Franc"

    @SerializedName("BMD")
    var BMD = "Bermudan Dollar"

    @SerializedName("BND")
    var BND = "Brunei Dollar"

    @SerializedName("BOB")
    var BOB = "Bolivian Boliviano"

    @SerializedName("BRL")
    var BRL = "Brazilian Real"

    @SerializedName("BSD")
    var BSD = "Bahamian Dollar"

    @SerializedName("BTC")
    var BTC = "Bitcoin"

    @SerializedName("BTN")
    var BTN = "Bhutanese Ngultrum"

    @SerializedName("BWP")
    var BWP = "Botswanan Pula"

    @SerializedName("BYN")
    var BYN = "New Belarusian Ruble"

    @SerializedName("BYR")
    var BYR = "Belarusian Ruble"

    @SerializedName("BZD")
    var BZD = "Belize Dollar"

    @SerializedName("CAD")
    var CAD = "Canadian Dollar"

    @SerializedName("CDF")
    var CDF = "Congolese Franc"

    @SerializedName("CHF")
    var CHF = "Swiss Franc"

    @SerializedName("CLF")
    var CLF = "Chilean Unit of Account (UF)"

    @SerializedName("CLP")
    var CLP = "Chilean Peso"

    @SerializedName("CNY")
    var CNY = "Chinese Yuan"

    @SerializedName("COP")
    var COP = "Colombian Peso"

    @SerializedName("CRC")
    var CRC = "Costa Rican Colón"

    @SerializedName("CUC")
    var CUC = "Cuban Convertible Peso"

    @SerializedName("CUP")
    var CUP = "Cuban Peso"

    @SerializedName("CVE")
    var CVE = "Cape Verdean Escudo"

    @SerializedName("CZK")
    var CZK = "Czech Republic Koruna"

    @SerializedName("DJF")
    var DJF = "Djiboutian Franc"

    @SerializedName("DKK")
    var DKK = "Danish Krone"

    @SerializedName("DOP")
    var DOP = "Dominican Peso"

    @SerializedName("DZD")
    var DZD = "Algerian Dinar"

    @SerializedName("EGP")
    var EGP = "Egyptian Pound"

    @SerializedName("ERN")
    var ERN = "Eritrean Nakfa"

    @SerializedName("ETB")
    var ETB = "Ethiopian Birr"

    @SerializedName("EUR")
    var EUR = "Euro"

    @SerializedName("FJD")
    var FJD = "Fijian Dollar"

    @SerializedName("FKP")
    var FKP = "Falkland Islands Pound"

    @SerializedName("GBP")
    var GBP = "British Pound Sterling"

    @SerializedName("GEL")
    var GEL = "Georgian Lari"

    @SerializedName("GGP")
    var GGP = "Guernsey Pound"

    @SerializedName("GHS")
    var GHS = "Ghanaian Cedi"

    @SerializedName("GIP")
    var GIP = "Gibraltar Pound"

    @SerializedName("GMD")
    var GMD = "Gambian Dalasi"

    @SerializedName("GNF")
    var GNF = "Guinean Franc"

    @SerializedName("GTQ")
    var GTQ = "Guatemalan Quetzal"

    @SerializedName("GYD")
    var GYD = "Guyanaese Dollar"

    @SerializedName("HKD")
    var HKD = "Hong Kong Dollar"

    @SerializedName("HNL")
    var HNL = "Honduran Lempira"

    @SerializedName("HRK")
    var HRK = "Croatian Kuna"

    @SerializedName("HTG")
    var HTG = "Haitian Gourde"

    @SerializedName("HUF")
    var HUF = "Hungarian Forint"

    @SerializedName("IDR")
    var IDR = "Indonesian Rupiah"

    @SerializedName("ILS")
    var ILS = "Israeli New Sheqel"

    @SerializedName("IMP")
    var IMP = "Manx pound"

    @SerializedName("INR")
    var INR = "Indian Rupee"

    @SerializedName("IQD")
    var IQD = "Iraqi Dinar"

    @SerializedName("IRR")
    var IRR = "Iranian Rial"

    @SerializedName("ISK")
    var ISK = "Icelandic Króna"

    @SerializedName("JEP")
    var JEP = "Jersey Pound"

    @SerializedName("JMD")
    var JMD = "Jamaican Dollar"

    @SerializedName("JOD")
    var JOD = "Jordanian Dinar"

    @SerializedName("JPY")
    var JPY = "Japanese Yen"

    @SerializedName("KES")
    var KES = "Kenyan Shilling"

    @SerializedName("KGS")
    var KGS = "Kyrgystani Som"

    @SerializedName("KHR")
    var KHR = "Cambodian Riel"

    @SerializedName("KMF")
    var KMF = "Comorian Franc"

    @SerializedName("KPW")
    var KPW = "North Korean Won"

    @SerializedName("KRW")
    var KRW = "South Korean Won"

    @SerializedName("KWD")
    var KWD = "Kuwaiti Dinar"

    @SerializedName("KYD")
    var KYD = "Cayman Islands Dollar"

    @SerializedName("KZT")
    var KZT = "Kazakhstani Tenge"

    @SerializedName("LAK")
    var LAK = "Laotian Kip"

    @SerializedName("LBP")
    var LBP = "Lebanese Pound"

    @SerializedName("LKR")
    var LKR = "Sri Lankan Rupee"

    @SerializedName("LRD")
    var LRD = "Liberian Dollar"

    @SerializedName("LSL")
    var LSL = "Lesotho Loti"

    @SerializedName("LTL")
    var LTL = "Lithuanian Litas"

    @SerializedName("LVL")
    var LVL = "Latvian Lats"

    @SerializedName("LYD")
    var LYD = "Libyan Dinar"

    @SerializedName("MAD")
    var MAD = "Moroccan Dirham"

    @SerializedName("MDL")
    var MDL = "Moldovan Leu"

    @SerializedName("MGA")
    var MGA = "Malagasy Ariary"

    @SerializedName("MKD")
    var MKD = "Macedonian Denar"

    @SerializedName("MMK")
    var MMK = "Myanma Kyat"

    @SerializedName("MNT")
    var MNT = "Mongolian Tugrik"

    @SerializedName("MOP")
    var MOP = "Macanese Pataca"

    @SerializedName("MRO")
    var MRO = "Mauritanian Ouguiya"

    @SerializedName("MUR")
    var MUR = "Mauritian Rupee"

    @SerializedName("MVR")
    var MVR = "Maldivian Rufiyaa"

    @SerializedName("MWK")
    var MWK = "Malawian Kwacha"

    @SerializedName("MXN")
    var MXN = "Mexican Peso"

    @SerializedName("MYR")
    var MYR = "Malaysian Ringgit"

    @SerializedName("MZN")
    var MZN = "Mozambican Metical"

    @SerializedName("NZD")
    var NZD = "New Zealand Dollar"

    @SerializedName("NOK")
    var NOK = "Norwegian Krone"


    @SerializedName("NAD")
    var NAD = "Namibian Dollar"

    @SerializedName("NGN")
    var NGN = "Nigerian Naira"

    @SerializedName("NIO")
    var NIO = "Nicaraguan Córdoba"

    @SerializedName("NPR")
    var NPR = "Nepalese Rupee"

    @SerializedName("OMR")
    var OMR = "Omani Rial"

    @SerializedName("PAB")
    var PAB = "Panamanian Balboa"

    @SerializedName("PEN")
    var PEN = "Peruvian Nuevo Sol"

    @SerializedName("PGK")
    var PGK = "Papua New Guinean Kina"

    @SerializedName("PHP")
    var PHP = "Philippine Peso"

    @SerializedName("PKR")
    var PKR = "Pakistani Rupee"

    @SerializedName("PLN")
    var PLN = "Polish Zloty"

    @SerializedName("PYG")
    var PYG = "Paraguayan Guarani"


    @SerializedName("RON")
    var RON = "Romanian Leu"

    @SerializedName("RSD")
    var RSD = "Serbian Dinar"

    @SerializedName("RWF")
    var RWF = "Rwandan Franc"

    @SerializedName("SAR")
    var SAR = "Saudi Riyal"

    @SerializedName("SBD")
    var SBD = "Solomon Islands Dollar"

    @SerializedName("SCR")
    var SCR = "Seychellois Rupee"

    @SerializedName("SDG")
    var SDG = "Sudanese Pound"

    @SerializedName("SHP")
    var SHP = "Saint Helena Pound"

    @SerializedName("SLL")
    var SLL = "Sierra Leonean Leone"

    @SerializedName("SOS")
    var SOS = "Somali Shilling"

    @SerializedName("SRD")
    var SRD = "Surinamese Dollar"

    @SerializedName("STD")
    var STD = "São Tomé and Príncipe Dobra"

    @SerializedName("SVC")
    var SVC = "Salvadoran Colón"

    @SerializedName("SYP")
    var SYP = "Syrian Pound"

    @SerializedName("SZL")
    var SZL = "Swazi Lilangeni"


    @SerializedName("QAR")
    var QAR = "Qatari Rial"

    @SerializedName("SGD")
    var SGD = "Singapore Dollar"

    @SerializedName("THB")
    var THB = "Thai Baht"

    @SerializedName("USD")
    var USD = "United States Dollar"

    @SerializedName("VND")
    var VND = "Vietnamese Dong"

    @SerializedName("RUB")
    var RUB = "Russian Ruble"

    @SerializedName("TJS")
    var TJS = "Tajikistani Somoni"

    @SerializedName("TMT")
    var TMT = "Turkmenistani Manat"

    @SerializedName("TND")
    var TND = "Tunisian Dinar"

    @SerializedName("TOP")
    var TOP = "Tongan Paʻanga"

    @SerializedName("TRY")
    var TRY = "Turkish Lira"

    @SerializedName("TTD")
    var TTD = "Trinidad and Tobago Dollar"

    @SerializedName("TZS")
    var TZS = "Tanzanian Shilling"

    @SerializedName("UAH")
    var UAH = "Ukrainian Hryvnia"

    @SerializedName("UGX")
    var UGX = "Ugandan Shilling"

    @SerializedName("UYU")
    var UYU = "Uruguayan Peso"

    @SerializedName("UZS")
    var UZS = "Uzbekistan Som"

    @SerializedName("VEF")
    var VEF = "Venezuelan Bolívar Fuerte"

    @SerializedName("VUV")
    var VUV = "Vanuatu Vatu"

    @SerializedName("WST")
    var WST = "Samoan Tala"

    @SerializedName("SEK")
    var SEK = "Swedish Krona"

    @SerializedName("TWD")
    var TWD = "New Taiwan Dollar"

    @SerializedName("ZAR")
    var ZAR = "South African Rand"

    @SerializedName("ZWL")
    var ZWL = "Zimbabwean Dollar"

    @SerializedName("XAF")
    var XAF = "CFA Franc BEAC"

    @SerializedName("XAG")
    var XAG = "Silver (troy ounce)"

    @SerializedName("XAU")
    var XAU = "Gold (troy ounce)"

    @SerializedName("XCD")
    var XCD = "East Caribbean Dollar"

    @SerializedName("XDR")
    var XDR = "Special Drawing Rights"

    @SerializedName("XOF")
    var XOF = "CFA Franc BCEAO"

    @SerializedName("XPF")
    var XPF = "CFP Franc"

    @SerializedName("YER")
    var YER = "Yemeni Rial"

    @SerializedName("ZMK")
    var ZMK = "Zambian Kwacha (pre-2013)"

    @SerializedName("ZMW")
    var ZMW = "Zambian Kwacha"

    fun getNameBySymbol(currency: String?): String {
        var result = ""
        when (currency) {
            "AED" -> result = AED
            "AFN" -> result = AFN
            "ALL" -> result = ALL
            "AMD" -> result = AMD
            "ANG" -> result = ANG
            "AOA" -> result = AOA
            "ARS" -> result = ARS
            "AUD" -> result = AUD
            "AWG" -> result = AWG
            "AZN" -> result = AZN
            "BAM" -> result = BAM
            "BBD" -> result = BBD
            "BDT" -> result = BDT
            "BGN" -> result = BGN
            "BHD" -> result = BHD
            "BIF" -> result = BIF
            "BMD" -> result = BMD
            "BND" -> result = BND
            "BOB" -> result = BOB
            "BRL" -> result = BRL
            "BSD" -> result = BSD
            "BTC" -> result = BTC
            "BTN" -> result = BTN
            "BWP" -> result = BWP
            "BYN" -> result = BYN
            "BYR" -> result = BYR
            "BZD" -> result = BZD
            "CAD" -> result = CAD
            "CDF" -> result = CDF
            "CHF" -> result = CHF
            "CLF" -> result = CLF
            "CLP" -> result = CLP
            "CNY" -> result = CNY
            "COP" -> result = COP
            "CRC" -> result = CRC
            "CUC" -> result = CUC
            "CUP" -> result = CUP
            "CVE" -> result = CVE
            "CZK" -> result = CZK
            "DJF" -> result = DJF
            "DKK" -> result = DKK
            "DOP" -> result = DOP
            "DZD" -> result = DZD
            "EGP" -> result = EGP
            "ERN" -> result = ERN
            "ETB" -> result = ETB
            "EUR" -> result = EUR
            "FJD" -> result = FJD
            "FKP" -> result = FKP
            "GBP" -> result = GBP
            "GEL" -> result = GEL
            "GGP" -> result = GGP
            "GHS" -> result = GHS
            "GIP" -> result = GIP
            "GMD" -> result = GMD
            "GNF" -> result = GNF
            "GTQ" -> result = GEL
            "GYD" -> result = GYD
            "HKD" -> result = HKD
            "HNL" -> result = HNL
            "HRK" -> result = HRK
            "HTG" -> result = HTG
            "HUF" -> result = HUF
            "IDR" -> result = IDR
            "ILS" -> result = ILS
            "IMP" -> result = IMP
            "INR" -> result = INR
            "IQD" -> result = IQD
            "IRR" -> result = IRR
            "ISK" -> result = ISK
            "JEP" -> result = JEP
            "JMD" -> result = JMD
            "JOD" -> result = JOD
            "JPY" -> result = JPY
            "KES" -> result = KES
            "KGS" -> result = KGS
            "KHR" -> result = KHR
            "KMF" -> result = KMF
            "KPW" -> result = KPW
            "KRW" -> result = KRW
            "KWD" -> result = KWD
            "KYD" -> result = KYD
            "KZT" -> result = KZT
            "LAK" -> result = LAK
            "LBP" -> result = LBP
            "LKR" -> result = LKR
            "LRD" -> result = LRD
            "LSL" -> result = LSL
            "LTL" -> result = LTL
            "LVL" -> result = LVL
            "LYD" -> result = LYD
            "MAD" -> result = MAD
            "MDL" -> result = MDL
            "MGA" -> result = MGA
            "MKD" -> result = MKD
            "MMK" -> result = MMK
            "MNT" -> result = MNT
            "MOP" -> result = MOP
            "MRO" -> result = MRO
            "MUR" -> result = MUR
            "MVR" -> result = MVR
            "MWK" -> result = MWK
            "MXN" -> result = MXN
            "MYR" -> result = MYR
            "MZN" -> result = MZN
            "NAD" -> result = NAD
            "NGN" -> result = NGN
            "NIO" -> result = NIO
            "NOK" -> result = NOK
            "NPR" -> result = NPR
            "NZD" -> result = NZD
            "OMR" -> result = OMR
            "PAB" -> result = PAB
            "PEN" -> result = PEN
            "PGK" -> result = PGK
            "PHP" -> result = PHP
            "PKR" -> result = PKR
            "PLN" -> result = PLN
            "PYG" -> result = PYG
            "QAR" -> result = QAR
            "RON" -> result = RON
            "RSD" -> result = RSD
            "RUB" -> result = RUB
            "RWF" -> result = RWF
            "SAR" -> result = SAR
            "SBD" -> result = SBD
            "SCR" -> result = SCR
            "SDG" -> result = SDG
            "SEK" -> result = SEK
            "SGD" -> result = SGD
            "SHP" -> result = SHP
            "SLL" -> result = SLL
            "SOS" -> result = SOS
            "SRD" -> result = SRD
            "STD" -> result = STD
            "SVC" -> result = SVC
            "SYP" -> result = SYP
            "SZL" -> result = SZL
            "THB" -> result = THB
            "TJS" -> result = TJS
            "TMT" -> result = TMT
            "TND" -> result = TND
            "TOP" -> result = TOP
            "TRY" -> result = TRY
            "TTD" -> result = TTD
            "TWD" -> result = TWD
            "TZS" -> result = TZS
            "UAH" -> result = UAH
            "UGX" -> result = UGX
            "USD" -> result = USD
            "UYU" -> result = UYU
            "UZS" -> result = UZS
            "VEF" -> result = VEF
            "VND" -> result = VND
            "VUV" -> result = VUV
            "WST" -> result = WST
            "XAF" -> result = XAF
            "XAG" -> result = XAG
            "XAU" -> result = XAU
            "XCD" -> result = XCD
            "XDR" -> result = XDR
            "XOF" -> result = XOF
            "XPF" -> result = XPF
            "YER" -> result = YER
            "ZAR" -> result = ZAR
            "ZMK" -> result = ZMK
            "ZMW" -> result = ZMW
            "ZWL" -> result = ZWL
        }
        return result
    }


    constructor(parcel: Parcel) : this() {
        AED = parcel.readString()!!
        AFN = parcel.readString()!!
        ALL = parcel.readString()!!
        AMD = parcel.readString()!!
        ANG = parcel.readString()!!
        AOA = parcel.readString()!!
        ARS = parcel.readString()!!
        AUD = parcel.readString()!!
        AWG = parcel.readString()!!
        AZN = parcel.readString()!!
        BAM = parcel.readString()!!
        BBD = parcel.readString()!!
        BDT = parcel.readString()!!
        BGN = parcel.readString()!!
        BHD = parcel.readString()!!
        BIF = parcel.readString()!!
        BMD = parcel.readString()!!
        BND = parcel.readString()!!
        BOB = parcel.readString()!!
        BRL = parcel.readString()!!
        BSD = parcel.readString()!!
        BTC = parcel.readString()!!
        BTN = parcel.readString()!!
        BWP = parcel.readString()!!
        BYN = parcel.readString()!!
        BYR = parcel.readString()!!
        BZD = parcel.readString()!!
        CAD = parcel.readString()!!
        CDF = parcel.readString()!!
        CHF = parcel.readString()!!
        CLF = parcel.readString()!!
        CLP = parcel.readString()!!
        CNY = parcel.readString()!!
        COP = parcel.readString()!!
        CRC = parcel.readString()!!
        CUC = parcel.readString()!!
        CUP = parcel.readString()!!
        CVE = parcel.readString()!!
        CZK = parcel.readString()!!
        DJF = parcel.readString()!!
        DKK = parcel.readString()!!
        DOP = parcel.readString()!!
        DZD = parcel.readString()!!
        EGP = parcel.readString()!!
        ERN = parcel.readString()!!
        ETB = parcel.readString()!!
        EUR = parcel.readString()!!
        FJD = parcel.readString()!!
        FKP = parcel.readString()!!
        GBP = parcel.readString()!!
        GEL = parcel.readString()!!
        GGP = parcel.readString()!!
        GHS = parcel.readString()!!
        GIP = parcel.readString()!!
        GMD = parcel.readString()!!
        GNF = parcel.readString()!!
        GTQ = parcel.readString()!!
        GYD = parcel.readString()!!
        HKD = parcel.readString()!!
        HNL = parcel.readString()!!
        HRK = parcel.readString()!!
        HTG = parcel.readString()!!
        HUF = parcel.readString()!!
        IDR = parcel.readString()!!
        ILS = parcel.readString()!!
        IMP = parcel.readString()!!
        INR = parcel.readString()!!
        IQD = parcel.readString()!!
        IRR = parcel.readString()!!
        ISK = parcel.readString()!!
        JEP = parcel.readString()!!
        JMD = parcel.readString()!!
        JOD = parcel.readString()!!
        JPY = parcel.readString()!!
        KES = parcel.readString()!!
        KGS = parcel.readString()!!
        KHR = parcel.readString()!!
        KMF = parcel.readString()!!
        KPW = parcel.readString()!!
        KRW = parcel.readString()!!
        KWD = parcel.readString()!!
        KYD = parcel.readString()!!
        KZT = parcel.readString()!!
        LAK = parcel.readString()!!
        LBP = parcel.readString()!!
        LKR = parcel.readString()!!
        LRD = parcel.readString()!!
        LSL = parcel.readString()!!
        LTL = parcel.readString()!!
        LVL = parcel.readString()!!
        LYD = parcel.readString()!!
        MAD = parcel.readString()!!
        MDL = parcel.readString()!!
        MGA = parcel.readString()!!
        MKD = parcel.readString()!!
        MMK = parcel.readString()!!
        MNT = parcel.readString()!!
        MOP = parcel.readString()!!
        MRO = parcel.readString()!!
        MUR = parcel.readString()!!
        MVR = parcel.readString()!!
        MWK = parcel.readString()!!
        MXN = parcel.readString()!!
        MYR = parcel.readString()!!
        MZN = parcel.readString()!!
        NZD = parcel.readString()!!
        NOK = parcel.readString()!!
        NAD = parcel.readString()!!
        NGN = parcel.readString()!!
        NIO = parcel.readString()!!
        NPR = parcel.readString()!!
        OMR = parcel.readString()!!
        PAB = parcel.readString()!!
        PEN = parcel.readString()!!
        PGK = parcel.readString()!!
        PHP = parcel.readString()!!
        PKR = parcel.readString()!!
        PLN = parcel.readString()!!
        PYG = parcel.readString()!!
        RON = parcel.readString()!!
        RSD = parcel.readString()!!
        RWF = parcel.readString()!!
        SAR = parcel.readString()!!
        SBD = parcel.readString()!!
        SCR = parcel.readString()!!
        SDG = parcel.readString()!!
        SHP = parcel.readString()!!
        SLL = parcel.readString()!!
        SOS = parcel.readString()!!
        SRD = parcel.readString()!!
        STD = parcel.readString()!!
        SVC = parcel.readString()!!
        SYP = parcel.readString()!!
        SZL = parcel.readString()!!
        QAR = parcel.readString()!!
        SGD = parcel.readString()!!
        THB = parcel.readString()!!
        USD = parcel.readString()!!
        VND = parcel.readString()!!
        RUB = parcel.readString()!!
        TJS = parcel.readString()!!
        TMT = parcel.readString()!!
        TND = parcel.readString()!!
        TOP = parcel.readString()!!
        TRY = parcel.readString()!!
        TTD = parcel.readString()!!
        TZS = parcel.readString()!!
        UAH = parcel.readString()!!
        UGX = parcel.readString()!!
        UYU = parcel.readString()!!
        UZS = parcel.readString()!!
        VEF = parcel.readString()!!
        VUV = parcel.readString()!!
        WST = parcel.readString()!!
        SEK = parcel.readString()!!
        TWD = parcel.readString()!!
        ZAR = parcel.readString()!!
        ZWL = parcel.readString()!!
        XAF = parcel.readString()!!
        XAG = parcel.readString()!!
        XAU = parcel.readString()!!
        XCD = parcel.readString()!!
        XDR = parcel.readString()!!
        XOF = parcel.readString()!!
        XPF = parcel.readString()!!
        YER = parcel.readString()!!
        ZMK = parcel.readString()!!
        ZMW = parcel.readString()!!
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel, p1: Int) {
        p0.writeString(AED)
        p0.writeString(AFN)
        p0.writeString(ALL)
        p0.writeString(AMD)
        p0.writeString(ANG)
        p0.writeString(AOA)
        p0.writeString(ARS)
        p0.writeString(AUD)
        p0.writeString(AWG)
        p0.writeString(AZN)
        p0.writeString(BAM)
        p0.writeString(BBD)
        p0.writeString(BDT)
        p0.writeString(BGN)
        p0.writeString(BHD)
        p0.writeString(BIF)
        p0.writeString(BMD)
        p0.writeString(BND)
        p0.writeString(BOB)
        p0.writeString(BRL)
        p0.writeString(BSD)
        p0.writeString(BTC)
        p0.writeString(BTN)
        p0.writeString(BWP)
        p0.writeString(BYN)
        p0.writeString(BYR)
        p0.writeString(BZD)
        p0.writeString(CAD)
        p0.writeString(CDF)
        p0.writeString(CHF)
        p0.writeString(CLF)
        p0.writeString(CLP)
        p0.writeString(CNY)
        p0.writeString(COP)
        p0.writeString(CRC)
        p0.writeString(CUC)
        p0.writeString(CUP)
        p0.writeString(CVE)
        p0.writeString(CZK)
        p0.writeString(DJF)
        p0.writeString(DKK)
        p0.writeString(DOP)
        p0.writeString(DZD)
        p0.writeString(EGP)
        p0.writeString(ERN)
        p0.writeString(ETB)
        p0.writeString(EUR)
        p0.writeString(FJD)
        p0.writeString(FKP)
        p0.writeString(GBP)
        p0.writeString(GEL)
        p0.writeString(GGP)
        p0.writeString(GHS)
        p0.writeString(GIP)
        p0.writeString(GMD)
        p0.writeString(GNF)
        p0.writeString(GTQ)
        p0.writeString(GYD)
        p0.writeString(HKD)
        p0.writeString(HNL)
        p0.writeString(HRK)
        p0.writeString(HTG)
        p0.writeString(HUF)
        p0.writeString(IDR)
        p0.writeString(ILS)
        p0.writeString(IMP)
        p0.writeString(INR)
        p0.writeString(IQD)
        p0.writeString(IRR)
        p0.writeString(ISK)
        p0.writeString(JEP)
        p0.writeString(JMD)
        p0.writeString(JOD)
        p0.writeString(JPY)
        p0.writeString(KES)
        p0.writeString(KGS)
        p0.writeString(KHR)
        p0.writeString(KMF)
        p0.writeString(KPW)
        p0.writeString(KRW)
        p0.writeString(KWD)
        p0.writeString(KYD)
        p0.writeString(KZT)
        p0.writeString(LAK)
        p0.writeString(LBP)
        p0.writeString(LKR)
        p0.writeString(LRD)
        p0.writeString(LSL)
        p0.writeString(LTL)
        p0.writeString(LVL)
        p0.writeString(LYD)
        p0.writeString(MAD)
        p0.writeString(MDL)
        p0.writeString(MGA)
        p0.writeString(MKD)
        p0.writeString(MMK)
        p0.writeString(MNT)
        p0.writeString(MOP)
        p0.writeString(MRO)
        p0.writeString(MUR)
        p0.writeString(MVR)
        p0.writeString(MWK)
        p0.writeString(MXN)
        p0.writeString(MYR)
        p0.writeString(MZN)
        p0.writeString(NZD)
        p0.writeString(NOK)
        p0.writeString(NAD)
        p0.writeString(NGN)
        p0.writeString(NIO)
        p0.writeString(NPR)
        p0.writeString(OMR)
        p0.writeString(PAB)
        p0.writeString(PEN)
        p0.writeString(PGK)
        p0.writeString(PHP)
        p0.writeString(PKR)
        p0.writeString(PLN)
        p0.writeString(PYG)
        p0.writeString(RON)
        p0.writeString(RSD)
        p0.writeString(RWF)
        p0.writeString(SAR)
        p0.writeString(SBD)
        p0.writeString(SCR)
        p0.writeString(SDG)
        p0.writeString(SHP)
        p0.writeString(SLL)
        p0.writeString(SOS)
        p0.writeString(SRD)
        p0.writeString(STD)
        p0.writeString(SVC)
        p0.writeString(SYP)
        p0.writeString(SZL)
        p0.writeString(QAR)
        p0.writeString(SGD)
        p0.writeString(THB)
        p0.writeString(USD)
        p0.writeString(VND)
        p0.writeString(RUB)
        p0.writeString(TJS)
        p0.writeString(TMT)
        p0.writeString(TND)
        p0.writeString(TOP)
        p0.writeString(TRY)
        p0.writeString(TTD)
        p0.writeString(TZS)
        p0.writeString(UAH)
        p0.writeString(UGX)
        p0.writeString(UYU)
        p0.writeString(UZS)
        p0.writeString(VEF)
        p0.writeString(VUV)
        p0.writeString(WST)
        p0.writeString(SEK)
        p0.writeString(TWD)
        p0.writeString(ZAR)
        p0.writeString(ZWL)
        p0.writeString(XAF)
        p0.writeString(XAG)
        p0.writeString(XAU)
        p0.writeString(XCD)
        p0.writeString(XDR)
        p0.writeString(XOF)
        p0.writeString(XPF)
        p0.writeString(YER)
        p0.writeString(ZMK)
        p0.writeString(ZMW)
    }

    companion object CREATOR : Parcelable.Creator<SymbolModel> {
        override fun createFromParcel(parcel: Parcel): SymbolModel {
            return SymbolModel(parcel)
        }

        override fun newArray(size: Int): Array<SymbolModel?> {
            return arrayOfNulls(size)
        }
    }

}