package com.testtestonly.ViddseeApp.Models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class CurrencyModel() : Parcelable {

    @SerializedName("AED")
    var AED: Float = 4.258543F

    @SerializedName("AFN")
    var AFN: Float = 104.565828F

    @SerializedName("ALL")
    var ALL: Float = 122.953271F

    @SerializedName("AMD")
    var AMD: Float = 554.367844F

    @SerializedName("ANG")
    var ANG: Float = 2.089797F

    @SerializedName("AOA")
    var AOA: Float = 692.153705F

    @SerializedName("ARS")
    var ARS: Float = 115.767267F

    @SerializedName("AUD")
    var AUD: Float = 1.556339F

    @SerializedName("AWG")
    var AWG: Float = 2.087476F

    @SerializedName("AZN")
    var AZN: Float = 1.970821F

    @SerializedName("BAM")
    var BAM: Float = 1.954518F

    @SerializedName("BBD")
    var BBD: Float = 2.32324F

    @SerializedName("BDT")
    var BDT: Float = 99.354502F

    @SerializedName("BGN")
    var BGN: Float = 1.955318F

    @SerializedName("BHD")
    var BHD: Float = 0.437118F

    @SerializedName("BIF")
    var BIF: Float = 2316.454596F

    @SerializedName("BMD")
    var BMD: Float = 1.159387F

    @SerializedName("BND")
    var BND: Float = 1.562751F

    @SerializedName("BOB")
    var BOB: Float = 7.994933F

    @SerializedName("BRL")
    var BRL: Float = 6.584966F

    @SerializedName("BSD")
    var BSD: Float = 1.159522F

    @SerializedName("BTC")
    var BTC: Float = 0.000018213449F

    @SerializedName("BTN")
    var BTN: Float = 86.628645F

    @SerializedName("BWP")
    var BWP: Float = 13.297573F

    @SerializedName("BYN")
    var BYN: Float = 2.848488F

    @SerializedName("BYR")
    var BYR: Float = 22723.979015F

    @SerializedName("BZD")
    var BZD: Float = 2.324839F

    @SerializedName("CAD")
    var CAD: Float = 1.436764F

    @SerializedName("CDF")
    var CDF: Float = 2332.686048F

    @SerializedName("CHF")
    var CHF: Float = 1.059877F

    @SerializedName("CLF")
    var CLF: Float = 0.034273F

    @SerializedName("CLP")
    var CLP: Float = 945.700247F

    @SerializedName("CNY")
    var CNY: Float = 7.420766F

    @SerializedName("COP")
    var COP: Float = 4383.62946F

    @SerializedName("CRC")
    var CRC: Float = 739.93327F

    @SerializedName("CUC")
    var CUC: Float = 1.159387F

    @SerializedName("CUP")
    var CUP: Float = 30.723747F

    @SerializedName("CVE")
    var CVE: Float = 110.72314F

    @SerializedName("CZK")
    var CZK: Float = 25.581057F

    @SerializedName("DJF")
    var DJF: Float = 206.451406F

    @SerializedName("DKK")
    var DKK: Float = 7.439842F

    @SerializedName("DOP")
    var DOP: Float = 65.447258F

    @SerializedName("DZD")
    var DZD: Float = 159.009699F

    @SerializedName("EGP")
    var EGP: Float = 18.225788F

    @SerializedName("ERN")
    var ERN: Float = 17.392237F

    @SerializedName("ETB")
    var ETB: Float = 54.780859F

    @SerializedName("EUR")
    var EUR: Float = 1.0F

    @SerializedName("FJD")
    var FJD: Float = 2.417346F

    @SerializedName("FKP")
    var FKP: Float = 0.850088F

    @SerializedName("GBP")
    var GBP: Float = 0.849841F

    @SerializedName("GEL")
    var GEL: Float = 3.669473F

    @SerializedName("GGP")
    var GGP: Float = 0.850088F

    @SerializedName("GHS")
    var GHS: Float = 7.07239F

    @SerializedName("GIP")
    var GIP: Float = 0.850088F

    @SerializedName("GMD")
    var GMD: Float = 60.28872F

    @SerializedName("GNF")
    var GNF: Float = 11130.112386F

    @SerializedName("GTQ")
    var GTQ: Float = 8.971277F

    @SerializedName("GYD")
    var GYD: Float = 242.737047F

    @SerializedName("HKD")
    var HKD: Float = 9.027731F

    @SerializedName("HNL")
    var HNL: Float = 28.010572F

    @SerializedName("HRK")
    var HRK: Float = 7.523148F

    @SerializedName("HTG")
    var HTG: Float = 113.83558F

    @SerializedName("HUF")
    var HUF: Float = 359.444341F

    @SerializedName("IDR")
    var IDR: Float = 16560.157677F

    @SerializedName("ILS")
    var ILS: Float = 3.63855F

    @SerializedName("IMP")
    var IMP: Float = 0.850088F

    @SerializedName("INR")
    var INR: Float = 86.526298F

    @SerializedName("IQD")
    var IQD: Float = 1692.704559F

    @SerializedName("IRR")
    var IRR: Float = 48972.493301F

    @SerializedName("ISK")
    var ISK: Float = 150.209857F

    @SerializedName("JEP")
    var JEP: Float = 0.850088F

    @SerializedName("JMD")
    var JMD: Float = 179.263657F

    @SerializedName("JOD")
    var JOD: Float = 0.822031F

    @SerializedName("JPY")
    var JPY: Float = 132.132958F

    @SerializedName("KES")
    var KES: Float = 128.982083F

    @SerializedName("KGS")
    var KGS: Float = 128.982083F

    @SerializedName("KHR")
    var KHR: Float = 4716.385148F

    @SerializedName("KMF")
    var KMF: Float = 492.304561F

    @SerializedName("KPW")
    var KPW: Float = 1043.447669F

    @SerializedName("KRW")
    var KRW: Float = 1364.30776F

    @SerializedName("KWD")
    var KWD: Float = 0.349891F

    @SerializedName("KYD")
    var KYD: Float = 0.966251F

    @SerializedName("KZT")
    var KZT: Float = 496.566647F

    @SerializedName("LAK")
    var LAK: Float = 11959.073642F

    @SerializedName("LBP")
    var LBP: Float = 1776.180163F

    @SerializedName("LKR")
    var LKR: Float = 234.225361F

    @SerializedName("LRD")
    var LRD: Float = 172.603726F

    @SerializedName("LSL")
    var LSL: Float = 16.672721F

    @SerializedName("LTL")
    var LTL: Float = 3.423367F

    @SerializedName("LVL")
    var LVL: Float = 0.701301F

    @SerializedName("LYD")
    var LYD: Float = 5.286992F

    @SerializedName("MAD")
    var MAD = 10.523171F

    @SerializedName("MDL")
    var MDL = 20.308866F

    @SerializedName("MGA")
    var MGA = 4596.968257F

    @SerializedName("MKD")
    var MKD = 61.573665F

    @SerializedName("MMK")
    var MMK = 2090.636621F

    @SerializedName("MNT")
    var MNT = 3305.323842F

    @SerializedName("MOP")
    var MOP = 9.29586F

    @SerializedName("MRO")
    var MRO = 413.900847F

    @SerializedName("MUR")
    var MUR = 50.456592F

    @SerializedName("MVR")
    var MVR = 17.912157F

    @SerializedName("MWK")
    var MWK = 946.059565F

    @SerializedName("MXN")
    var MXN = 24.068578F

    @SerializedName("MYR")
    var MYR = 4.809714F

    @SerializedName("MZN")
    var MZN = 74.003311F

    @SerializedName("NZD")
    var NZD: Float = 1.630341F

    @SerializedName("NOK")
    var NOK: Float = 9.768255F

    @SerializedName("NAD")
    var NAD = 16.67216F

    @SerializedName("NGN")
    var NGN = 475.835703F

    @SerializedName("NIO")
    var NIO = 40.833213F

    @SerializedName("NPR")
    var NPR = 138.605952F

    @SerializedName("OMR")
    var OMR = 0.446381F

    @SerializedName("PAB")
    var PAB = 1.159522F

    @SerializedName("PEN")
    var PEN = 4.630623F

    @SerializedName("PGK")
    var PGK = 4.092583F

    @SerializedName("PHP")
    var PHP = 58.606418F

    @SerializedName("PKR")
    var PKR = 198.777194F

    @SerializedName("PLN")
    var PLN = 4.607227F

    @SerializedName("PYG")
    var PYG = 8002.52777F


    @SerializedName("RON")
    var RON = 4.950952F

    @SerializedName("RSD")
    var RSD = 117.521106F

    @SerializedName("RWF")
    var RWF = 1159.386684F

    @SerializedName("SAR")
    var SAR = 4.348893F

    @SerializedName("SBD")
    var SBD = 9.304923F

    @SerializedName("SCR")
    var SCR = 15.30889F

    @SerializedName("SDG")
    var SDG = 510.706732F

    @SerializedName("SHP")
    var SHP = 1.59694F

    @SerializedName("SLL")
    var SLL = 12608.330146F

    @SerializedName("SOS")
    var SOS = 679.401061F

    @SerializedName("SRD")
    var SRD = 25.043331F

    @SerializedName("STD")
    var STD = 23996.963576F

    @SerializedName("SVC")
    var SVC = 10.146189F

    @SerializedName("SYP")
    var SYP = 1457.316627F

    @SerializedName("SZL")
    var SZL = 16.671563F


    @SerializedName("QAR")
    var QAR: Float = 4.224181F

    @SerializedName("SGD")
    var SGD: Float = 1.562928F

    @SerializedName("THB")
    var THB: Float = 38.679769F

    @SerializedName("USD")
    var USD: Float = 1.16016F

    @SerializedName("VND")
    var VND: Float = 26392.490058F

    @SerializedName("RUB")
    var RUB: Float = 82.987158F

    @SerializedName("TJS")
    var TJS: Float = 13.021558F

    @SerializedName("TMT")
    var TMT: Float = 4.057853F

    @SerializedName("TND")
    var TND: Float = 3.28628F

    @SerializedName("TOP")
    var TOP: Float = 2.591751F

    @SerializedName("TRY")
    var TRY: Float = 11.11771F

    @SerializedName("TTD")
    var TTD: Float = 7.862122F

    @SerializedName("TZS")
    var TZS: Float = 2670.067065F

    @SerializedName("UAH")
    var UAH: Float = 30.495128F

    @SerializedName("UGX")
    var UGX: Float = 4120.333951F

    @SerializedName("UYU")
    var UYU: Float = 51.245598F

    @SerializedName("UZS")
    var UZS: Float = 12405.437757F

    @SerializedName("VEF")
    var VEF: Float = 247911912510.44647F

    @SerializedName("VUV")
    var VUV: Float = 130.194989F

    @SerializedName("WST")
    var WST: Float = 2.987731F

    @SerializedName("SEK")
    var SEK: Float = 9.917765F

    @SerializedName("TWD")
    var TWD: Float = 32.295299F

    @SerializedName("ZAR")
    var ZAR: Float = 17.880988F

    @SerializedName("ZWL")
    var ZWL: Float = 373.322039F

    @SerializedName("XAF")
    var XAF = 655.516941F

    @SerializedName("XAG")
    var XAG = 0.049438F

    @SerializedName("XAU")
    var XAU = 0.000648F

    @SerializedName("XCD")
    var XCD = 3.133301F

    @SerializedName("XDR")
    var XDR = 0.821848F

    @SerializedName("XOF")
    var XOF = 650.993236F

    @SerializedName("XPF")
    var XPF = 119.938164F

    @SerializedName("YER")
    var YER = 290.1368F

    @SerializedName("ZMK")
    var ZMK = 10435.928687F

    @SerializedName("ZMW")
    var ZMW = 20.077322F

    fun getRateByCurrency(currency: String): Float {
        var result = 1.0F
        when (currency) {
            "AED" -> result = AED
            "AFN" -> result = AFN
            "ALL" -> result = ALL
            "AMD" -> result = AMD
            "ANG" -> result = ANG
            "AOA" -> result = AOA
            "ARS" -> result = ARS
            "AUD" -> result = AUD
            "AWG" -> result = AWG
            "AZN" -> result = AZN
            "BAM" -> result = BAM
            "BBD" -> result = BBD
            "BDT" -> result = BDT
            "BGN" -> result = BGN
            "BHD" -> result = BHD
            "BIF" -> result = BIF
            "BMD" -> result = BMD
            "BND" -> result = BND
            "BOB" -> result = BOB
            "BRL" -> result = BRL
            "BSD" -> result = BSD
            "BTC" -> result = BTC
            "BTN" -> result = BTN
            "BWP" -> result = BWP
            "BYN" -> result = BYN
            "BYR" -> result = BYR
            "BZD" -> result = BZD
            "CAD" -> result = CAD
            "CDF" -> result = CDF
            "CHF" -> result = CHF
            "CLF" -> result = CLF
            "CLP" -> result = CLP
            "CNY" -> result = CNY
            "COP" -> result = COP
            "CRC" -> result = CRC
            "CUC" -> result = CUC
            "CUP" -> result = CUP
            "CVE" -> result = CVE
            "CZK" -> result = CZK
            "DJF" -> result = DJF
            "DKK" -> result = DKK
            "DOP" -> result = DOP
            "DZD" -> result = DZD
            "EGP" -> result = EGP
            "ERN" -> result = ERN
            "ETB" -> result = ETB
            "EUR" -> result = EUR
            "FJD" -> result = FJD
            "FKP" -> result = FKP
            "GBP" -> result = GBP
            "GEL" -> result = GEL
            "GGP" -> result = GGP
            "GHS" -> result = GHS
            "GIP" -> result = GIP
            "GMD" -> result = GMD
            "GNF" -> result = GNF
            "GTQ" -> result = GEL
            "GYD" -> result = GYD
            "HKD" -> result = HKD
            "HNL" -> result = HNL
            "HRK" -> result = HRK
            "HTG" -> result = HTG
            "HUF" -> result = HUF
            "IDR" -> result = IDR
            "ILS" -> result = ILS
            "IMP" -> result = IMP
            "INR" -> result = INR
            "IQD" -> result = IQD
            "IRR" -> result = IRR
            "ISK" -> result = ISK
            "JEP" -> result = JEP
            "JMD" -> result = JMD
            "JOD" -> result = JOD
            "JPY" -> result = JPY
            "KES" -> result = KES
            "KGS" -> result = KGS
            "KHR" -> result = KHR
            "KMF" -> result = KMF
            "KPW" -> result = KPW
            "KRW" -> result = KRW
            "KWD" -> result = KWD
            "KYD" -> result = KYD
            "KZT" -> result = KZT
            "LAK" -> result = LAK
            "LBP" -> result = LBP
            "LKR" -> result = LKR
            "LRD" -> result = LRD
            "LSL" -> result = LSL
            "LTL" -> result = LTL
            "LVL" -> result = LVL
            "LYD" -> result = LYD
            "MAD" -> result = MAD
            "MDL" -> result = MDL
            "MGA" -> result = MGA
            "MKD" -> result = MKD
            "MMK" -> result = MMK
            "MNT" -> result = MNT
            "MOP" -> result = MOP
            "MRO" -> result = MRO
            "MUR" -> result = MUR
            "MVR" -> result = MVR
            "MWK" -> result = MWK
            "MXN" -> result = MXN
            "MYR" -> result = MYR
            "MZN" -> result = MZN
            "NAD" -> result = NAD
            "NGN" -> result = NGN
            "NIO" -> result = NIO
            "NOK" -> result = NOK
            "NPR" -> result = NPR
            "NZD" -> result = NZD
            "OMR" -> result = OMR
            "PAB" -> result = PAB
            "PEN" -> result = PEN
            "PGK" -> result = PGK
            "PHP" -> result = PHP
            "PKR" -> result = PKR
            "PLN" -> result = PLN
            "PYG" -> result = PYG
            "QAR" -> result = QAR
            "RON" -> result = RON
            "RSD" -> result = RSD
            "RUB" -> result = RUB
            "RWF" -> result = RWF
            "SAR" -> result = SAR
            "SBD" -> result = SBD
            "SCR" -> result = SCR
            "SDG" -> result = SDG
            "SEK" -> result = SEK
            "SGD" -> result = SGD
            "SHP" -> result = SHP
            "SLL" -> result = SLL
            "SOS" -> result = SOS
            "SRD" -> result = SRD
            "STD" -> result = STD
            "SVC" -> result = SVC
            "SYP" -> result = SYP
            "SZL" -> result = SZL
            "THB" -> result = THB
            "TJS" -> result = TJS
            "TMT" -> result = TMT
            "TND" -> result = TND
            "TOP" -> result = TOP
            "TRY" -> result = TRY
            "TTD" -> result = TTD
            "TWD" -> result = TWD
            "TZS" -> result = TZS
            "UAH" -> result = UAH
            "UGX" -> result = UGX
            "USD" -> result = USD
            "UYU" -> result = UYU
            "UZS" -> result = UZS
            "VEF" -> result = VEF
            "VND" -> result = VND
            "VUV" -> result = VUV
            "WST" -> result = WST
            "XAF" -> result = XAF
            "XAG" -> result = XAG
            "XAU" -> result = XAU
            "XCD" -> result = XCD
            "XDR" -> result = XDR
            "XOF" -> result = XOF
            "XPF" -> result = XPF
            "YER" -> result = YER
            "ZAR" -> result = ZAR
            "ZMK" -> result = ZMK
            "ZMW" -> result = ZMW
            "ZWL" -> result = ZWL
        }
        return result
    }


    constructor(parcel: Parcel) : this() {
        AED = parcel.readFloat()
        AFN = parcel.readFloat()
        ALL = parcel.readFloat()
        AMD = parcel.readFloat()
        ANG = parcel.readFloat()
        AOA = parcel.readFloat()
        ARS = parcel.readFloat()
        AUD = parcel.readFloat()
        AWG = parcel.readFloat()
        AZN = parcel.readFloat()
        BAM = parcel.readFloat()
        BBD = parcel.readFloat()
        BDT = parcel.readFloat()
        BGN = parcel.readFloat()
        BHD = parcel.readFloat()
        BIF = parcel.readFloat()
        BMD = parcel.readFloat()
        BND = parcel.readFloat()
        BOB = parcel.readFloat()
        BRL = parcel.readFloat()
        BSD = parcel.readFloat()
        BTC = parcel.readFloat()
        BTN = parcel.readFloat()
        BWP = parcel.readFloat()
        BYN = parcel.readFloat()
        BYR = parcel.readFloat()
        BZD = parcel.readFloat()
        CAD = parcel.readFloat()
        CDF = parcel.readFloat()
        CHF = parcel.readFloat()
        CLF = parcel.readFloat()
        CLP = parcel.readFloat()
        CNY = parcel.readFloat()
        COP = parcel.readFloat()
        CRC = parcel.readFloat()
        CUC = parcel.readFloat()
        CUP = parcel.readFloat()
        CVE = parcel.readFloat()
        CZK = parcel.readFloat()
        DJF = parcel.readFloat()
        DKK = parcel.readFloat()
        DOP = parcel.readFloat()
        DZD = parcel.readFloat()
        EGP = parcel.readFloat()
        ERN = parcel.readFloat()
        ETB = parcel.readFloat()
        EUR = parcel.readFloat()
        FJD = parcel.readFloat()
        FKP = parcel.readFloat()
        GBP = parcel.readFloat()
        GEL = parcel.readFloat()
        GGP = parcel.readFloat()
        GHS = parcel.readFloat()
        GIP = parcel.readFloat()
        GMD = parcel.readFloat()
        GNF = parcel.readFloat()
        GTQ = parcel.readFloat()
        GYD = parcel.readFloat()
        HKD = parcel.readFloat()
        HNL = parcel.readFloat()
        HRK = parcel.readFloat()
        HTG = parcel.readFloat()
        HUF = parcel.readFloat()
        IDR = parcel.readFloat()
        ILS = parcel.readFloat()
        IMP = parcel.readFloat()
        INR = parcel.readFloat()
        IQD = parcel.readFloat()
        IRR = parcel.readFloat()
        ISK = parcel.readFloat()
        JEP = parcel.readFloat()
        JMD = parcel.readFloat()
        JOD = parcel.readFloat()
        JPY = parcel.readFloat()
        KES = parcel.readFloat()
        KGS = parcel.readFloat()
        KHR = parcel.readFloat()
        KMF = parcel.readFloat()
        KPW = parcel.readFloat()
        KRW = parcel.readFloat()
        KWD = parcel.readFloat()
        KYD = parcel.readFloat()
        KZT = parcel.readFloat()
        LAK = parcel.readFloat()
        LBP = parcel.readFloat()
        LKR = parcel.readFloat()
        LRD = parcel.readFloat()
        LAK = parcel.readFloat()
        LBP = parcel.readFloat()
        LKR = parcel.readFloat()
        LRD = parcel.readFloat()
        LSL = parcel.readFloat()
        LTL = parcel.readFloat()
        LVL = parcel.readFloat()
        LYD = parcel.readFloat()
        MAD = parcel.readFloat()
        MDL = parcel.readFloat()
        MGA = parcel.readFloat()
        MKD = parcel.readFloat()
        MMK = parcel.readFloat()
        MNT = parcel.readFloat()
        MOP = parcel.readFloat()
        MRO = parcel.readFloat()
        MUR = parcel.readFloat()
        MVR = parcel.readFloat()
        MWK = parcel.readFloat()
        MXN = parcel.readFloat()
        MYR = parcel.readFloat()
        MZN = parcel.readFloat()
        NZD = parcel.readFloat()
        NOK = parcel.readFloat()
        NAD = parcel.readFloat()
        NGN = parcel.readFloat()
        NIO = parcel.readFloat()
        NPR = parcel.readFloat()
        OMR = parcel.readFloat()
        PAB = parcel.readFloat()
        PEN = parcel.readFloat()
        PGK = parcel.readFloat()
        PHP = parcel.readFloat()
        PKR = parcel.readFloat()
        PLN = parcel.readFloat()
        PYG = parcel.readFloat()
        RON = parcel.readFloat()
        RSD = parcel.readFloat()
        RWF = parcel.readFloat()
        SAR = parcel.readFloat()
        SBD = parcel.readFloat()
        SCR = parcel.readFloat()
        SDG = parcel.readFloat()
        SHP = parcel.readFloat()
        SLL = parcel.readFloat()
        SOS = parcel.readFloat()
        SRD = parcel.readFloat()
        STD = parcel.readFloat()
        SVC = parcel.readFloat()
        SYP = parcel.readFloat()
        SZL = parcel.readFloat()
        QAR = parcel.readFloat()
        SGD = parcel.readFloat()
        THB = parcel.readFloat()
        USD = parcel.readFloat()
        VND = parcel.readFloat()
        RUB = parcel.readFloat()
        TJS = parcel.readFloat()
        TMT = parcel.readFloat()
        TND = parcel.readFloat()
        TOP = parcel.readFloat()
        TRY = parcel.readFloat()
        TTD = parcel.readFloat()
        TZS = parcel.readFloat()
        UAH = parcel.readFloat()
        UGX = parcel.readFloat()
        UYU = parcel.readFloat()
        UZS = parcel.readFloat()
        VEF = parcel.readFloat()
        VUV = parcel.readFloat()
        WST = parcel.readFloat()
        SEK = parcel.readFloat()
        TWD = parcel.readFloat()
        ZAR = parcel.readFloat()
        ZWL = parcel.readFloat()
        XAF = parcel.readFloat()
        XAG = parcel.readFloat()
        XAU = parcel.readFloat()
        XCD = parcel.readFloat()
        XDR = parcel.readFloat()
        XOF = parcel.readFloat()
        XPF = parcel.readFloat()
        YER = parcel.readFloat()
        ZMK = parcel.readFloat()
        ZMW = parcel.readFloat()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(AED)
        parcel.writeFloat(AFN)
        parcel.writeFloat(ALL)
        parcel.writeFloat(AMD)
        parcel.writeFloat(ANG)
        parcel.writeFloat(AOA)
        parcel.writeFloat(ARS)
        parcel.writeFloat(AUD)
        parcel.writeFloat(AWG)
        parcel.writeFloat(AZN)
        parcel.writeFloat(BAM)
        parcel.writeFloat(BBD)
        parcel.writeFloat(BDT)
        parcel.writeFloat(BGN)
        parcel.writeFloat(BHD)
        parcel.writeFloat(BIF)
        parcel.writeFloat(BMD)
        parcel.writeFloat(BND)
        parcel.writeFloat(BOB)
        parcel.writeFloat(BRL)
        parcel.writeFloat(BSD)
        parcel.writeFloat(BTC)
        parcel.writeFloat(BTN)
        parcel.writeFloat(BWP)
        parcel.writeFloat(BYN)
        parcel.writeFloat(BYR)
        parcel.writeFloat(BZD)
        parcel.writeFloat(CAD)
        parcel.writeFloat(CDF)
        parcel.writeFloat(CHF)
        parcel.writeFloat(CLF)
        parcel.writeFloat(CLP)
        parcel.writeFloat(CNY)
        parcel.writeFloat(COP)
        parcel.writeFloat(CRC)
        parcel.writeFloat(CUC)
        parcel.writeFloat(CUP)
        parcel.writeFloat(CVE)
        parcel.writeFloat(CZK)
        parcel.writeFloat(DJF)
        parcel.writeFloat(DKK)
        parcel.writeFloat(DOP)
        parcel.writeFloat(DZD)
        parcel.writeFloat(EGP)
        parcel.writeFloat(ERN)
        parcel.writeFloat(ETB)
        parcel.writeFloat(EUR)
        parcel.writeFloat(FJD)
        parcel.writeFloat(FKP)
        parcel.writeFloat(GBP)
        parcel.writeFloat(GEL)
        parcel.writeFloat(GGP)
        parcel.writeFloat(GHS)
        parcel.writeFloat(GIP)
        parcel.writeFloat(GMD)
        parcel.writeFloat(GNF)
        parcel.writeFloat(GTQ)
        parcel.writeFloat(GYD)
        parcel.writeFloat(HKD)
        parcel.writeFloat(HNL)
        parcel.writeFloat(HRK)
        parcel.writeFloat(HTG)
        parcel.writeFloat(HUF)
        parcel.writeFloat(IDR)
        parcel.writeFloat(ILS)
        parcel.writeFloat(IMP)
        parcel.writeFloat(INR)
        parcel.writeFloat(IQD)
        parcel.writeFloat(IRR)
        parcel.writeFloat(ISK)
        parcel.writeFloat(JEP)
        parcel.writeFloat(JMD)
        parcel.writeFloat(JOD)
        parcel.writeFloat(JPY)
        parcel.writeFloat(KES)
        parcel.writeFloat(KGS)
        parcel.writeFloat(KHR)
        parcel.writeFloat(KMF)
        parcel.writeFloat(KPW)
        parcel.writeFloat(KRW)
        parcel.writeFloat(KWD)
        parcel.writeFloat(KYD)
        parcel.writeFloat(KZT)
        parcel.writeFloat(LAK)
        parcel.writeFloat(LBP)
        parcel.writeFloat(LKR)
        parcel.writeFloat(LRD)
        parcel.writeFloat(LAK)
        parcel.writeFloat(LBP)
        parcel.writeFloat(LKR)
        parcel.writeFloat(LRD)
        parcel.writeFloat(LSL)
        parcel.writeFloat(LTL)
        parcel.writeFloat(LVL)
        parcel.writeFloat(LYD)
        parcel.writeFloat(MAD)
        parcel.writeFloat(MDL)
        parcel.writeFloat(MGA)
        parcel.writeFloat(MKD)
        parcel.writeFloat(MMK)
        parcel.writeFloat(MNT)
        parcel.writeFloat(MOP)
        parcel.writeFloat(MRO)
        parcel.writeFloat(MUR)
        parcel.writeFloat(MVR)
        parcel.writeFloat(MWK)
        parcel.writeFloat(MXN)
        parcel.writeFloat(MYR)
        parcel.writeFloat(MZN)
        parcel.writeFloat(NZD)
        parcel.writeFloat(NOK)
        parcel.writeFloat(NAD)
        parcel.writeFloat(NGN)
        parcel.writeFloat(NIO)
        parcel.writeFloat(NPR)
        parcel.writeFloat(OMR)
        parcel.writeFloat(PAB)
        parcel.writeFloat(PEN)
        parcel.writeFloat(PGK)
        parcel.writeFloat(PHP)
        parcel.writeFloat(PKR)
        parcel.writeFloat(PLN)
        parcel.writeFloat(PYG)
        parcel.writeFloat(RON)
        parcel.writeFloat(RSD)
        parcel.writeFloat(RWF)
        parcel.writeFloat(SAR)
        parcel.writeFloat(SBD)
        parcel.writeFloat(SCR)
        parcel.writeFloat(SDG)
        parcel.writeFloat(SHP)
        parcel.writeFloat(SLL)
        parcel.writeFloat(SOS)
        parcel.writeFloat(SRD)
        parcel.writeFloat(STD)
        parcel.writeFloat(SVC)
        parcel.writeFloat(SYP)
        parcel.writeFloat(SZL)
        parcel.writeFloat(QAR)
        parcel.writeFloat(SGD)
        parcel.writeFloat(THB)
        parcel.writeFloat(USD)
        parcel.writeFloat(VND)
        parcel.writeFloat(RUB)
        parcel.writeFloat(TJS)
        parcel.writeFloat(TMT)
        parcel.writeFloat(TND)
        parcel.writeFloat(TOP)
        parcel.writeFloat(TRY)
        parcel.writeFloat(TTD)
        parcel.writeFloat(TZS)
        parcel.writeFloat(UAH)
        parcel.writeFloat(UGX)
        parcel.writeFloat(UYU)
        parcel.writeFloat(UZS)
        parcel.writeFloat(VEF)
        parcel.writeFloat(VUV)
        parcel.writeFloat(WST)
        parcel.writeFloat(SEK)
        parcel.writeFloat(TWD)
        parcel.writeFloat(ZAR)
        parcel.writeFloat(ZWL)
        parcel.writeFloat(XAF)
        parcel.writeFloat(XAG)
        parcel.writeFloat(XAU)
        parcel.writeFloat(XCD)
        parcel.writeFloat(XDR)
        parcel.writeFloat(XOF)
        parcel.writeFloat(XPF)
        parcel.writeFloat(YER)
        parcel.writeFloat(ZMK)
        parcel.writeFloat(ZMW)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CurrencyModel> {
        override fun createFromParcel(parcel: Parcel): CurrencyModel {
            return CurrencyModel(parcel)
        }

        override fun newArray(size: Int): Array<CurrencyModel?> {
            return arrayOfNulls(size)
        }
    }
}