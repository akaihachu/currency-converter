# Kotlin MVP currency converter
1. What code architecture did you choose and why?
- MVC, MVP and MVVM are all acceptable to use but I use MVP pattern to code this app. MVP is tidy architecture and we can use it quite efficiently to deal with an app with a lot of local data calculation like this app. The MVVP is a good choice but it only show the advanced behaviors with an app using a lot data from RESTful API (sever side).


2. Further improvements to the code, if any, that you were not able to include in time
- Better design is necessary for this app. I just make a simple type design that is not very convenient for users.
- The number of currencies used in the app is 20 and We can use more currencies or all the currencies in the list.

3. Decision points:
- Kotlin is the language I choose because it is more flexible to customize and it has got more options to select to deal with. In common word, Kotlin make the code more briefly.
- First, I use the currency rates from http://api.exchangeratesapi.io/v1/latest?access_key=59ee8f035ba56129fb06bb2d9606e273 as your recommendation and it is quite a good and accurate data (But I am not sure when the limit of 250 downloads a month will come to).The currency detail (i.e "Singapore Dollar" for symbol SGD)can be load from API link: http://api.exchangeratesapi.io/v1/symbols?access_key=59ee8f035ba56129fb06bb2d9606e273
- The start amount length is 18, it means that the largest number it can put in is 99,999,999,999,999. The text size of this textview is constant. We can make this length longer but it can break UIs or cause error (for example the exchanging amount is larger than the Maximum of Double or Long)
- The exchanging amount is calculated so we don't know it maximum length. It can be either very large very little. So we give it a default 2 digits for decimal parts (the cent amount) and resize the textsize in order to keep the UIs.
- The CLEAR button and KEYBOARD can be removed from the design but it is kept to make the app more convenient to use. The user can clear the amounts and hide/unhide the keyboard more easily.
- When we first install the app, the currency rates data is available as constant in the code (copied on 4th of November, 2021). If the app is online, it will update the data right away to phone disc (using Sharedpreference) and use it later. And whenever the user open the home app, it will check to update and save disc the latest currency rates data (update if the previous currency rates data is 1 hour old, and currency detail is 10 hours old).
- To change the currency, we launch a new Activity with a RecycleView to display the list of currencies user can select. RecycleView is easy to customize and display better UI in compare to Spinner or other list selection tool.
- We use AppCompatTextView for exchanging amount to make it able to resize.
- We use Cardview for item of RecycleView
- We use Retrofit/HttpOK3 to load data from sever, it is the quickest and most reliable libraries to deal with Rest API.
- We store data with Sharedpreference and Gson (to turn an android Model to string). Sharedpreference is most common tool to save simple data to phone disc.
- We use Androidx because it is compatible with many common libraries.
- MinSdk 21 - Anroid 5.0 or LOLLIPOP so that 98,6% mobile devices can use the app, so that many old mobile phones can use.





